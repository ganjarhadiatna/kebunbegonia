<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use App\NoteModel;

class NoteController extends Controller
{
    //private
    function index()
    {
        $note = NoteModel::AllNote(12, 'desc');
        return view('admin.note.index', [
            'path' => 'note',
            'title' => 'Daftar Galeri',
            'note' => $note
        ]);
    }
    function create()
    {
        return view('admin.note.create', [
            'path' => 'note'
        ]);
    }
    function edit($idnote)
    {
        $note = NoteModel::noteById($idnote);
        return view('admin.note.edit', [
            'path' => 'note',
            'note' => $note,
        ]);
    }

    //post
    function publish(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            if ($req->hasFile('cover')) 
            {
                $checkFile = $this->validate($req, [
                    'cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:50000',
                ]);

                if ($checkFile) 
                {
                    $cover = $req->file('cover');
        
                    $chrc = array(
                        '[',']','@',' ','+','-','#','*',
                        '<','>','_','(',')',';',',','&',
                        '%','$','!','`','~','=','{','}',
                        '/',':','?','"',"'",'^'
                    );
                    $filename = $id.time().str_replace($chrc, '', $cover->getClientOriginalName());
        
                    //create thumbnail
                    $destination = public_path('img/note/thumbnails/'.$filename);
                    $img = Image::make($cover->getRealPath());

                    $thumbnail = $img->resize(600, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destination);
        
                    //create image real
                    $destination = public_path('img/note/covers/');
                    $real = $cover->move($destination, $filename); 

                    if ($thumbnail && $real) 
                    {
                        $cvrName = $filename;
                        $title = $req['title'];
                        $description = $req['description'];
                        $link = $req['link'];
                        if ($req['service'] != '0') 
                        {
                            $service = $req['service'];
                        } 
                        else 
                        {
                            $service = NULL;
                        }

                        $data = [
                            'cover' => $cvrName,
                            'icon' => $req['icon'],
                            'title' => $title,
                            'description' => $description,
                            'link' => $link,
                            'id' => $id
                        ];

                        $sql = NoteModel::Insert($data);
                        if ($sql) 
                        {
                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload note success',
                            ]);
                        } 
                        else 
                        {
                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload note failed',
                            ]);
                        }

                    } 
                    else 
                    {
                        return json_encode([
                            'status' => 'error',
                            'message' => 'Upload cover failed',
                        ]);
                    }
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Cover not valid',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Please choose one cover',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function put(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idnote = $req['idnote'];
            $title = $req['title'];
            $description = $req['description'];
            $link = $req['link'];

            $data = [
                'title' => $title,
                'icon' => $req['icon'],
                'description' => $description,
                'link' => $link,
                'id' => $id
            ];

            $sql = NoteModel::Edit($data, $idnote);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited note success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited note failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function remove(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idnote = $req['idnote'];

            //getting image
            $image = NoteModel::NoteImageById($idnote);

            if (true) 
            {
                //remove database
                $sql = NoteModel::Remove($idnote);
                if ($sql) 
                {
                    //remove image
                    $rmvThumbnail = unlink(public_path('img/note/thumbnails/'.$image));
                    $rmvCover = unlink(public_path('img/note/covers/'.$image));
                    return json_encode([
                        'status' => 'success',
                        'message' => 'Delete note success',
                    ]);
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Delete note failed',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Delete cover failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }
    }
}
