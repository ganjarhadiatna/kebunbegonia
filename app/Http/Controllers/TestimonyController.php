<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use App\TestimonyModel;

class TestimonyController extends Controller
{
    //private
    function index()
    {
        $testimony = TestimonyModel::AllTestimony(10, 'desc');
        return view('admin.testimony.index', [
            'path' => 'testimony',
            'testimony' => $testimony
        ]);
    }
    function create()
    {
        return view('admin.testimony.create', [
            'path' => 'testimony'
        ]);
    }
    function edit($idtestimony)
    {
        $testimony = TestimonyModel::TestimonyById($idtestimony);
        return view('admin.testimony.edit', [
            'path' => 'testimony',
            'testimony' => $testimony
        ]);
    }

    //post
    function publish(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            if (true) 
            {

                if (true) 
                {

                    if (true) 
                    {
                        $name = $req['name'];
                        $job = $req['job'];
                        $response = $req['response'];

                        $data = [
                            'name' => $name,
                            'job' => $job,
                            'response' => $response,
                            'id' => $id
                        ];

                        $sql = TestimonyModel::Insert($data);
                        if ($sql) 
                        {
                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload testimony success',
                            ]);
                        } 
                        else 
                        {
                            return json_encode([
                                'status' => 'success',
                                'message' => 'Upload testimony failed',
                            ]);
                        }

                    } 
                    else 
                    {
                        return json_encode([
                            'status' => 'error',
                            'message' => 'Upload photo failed',
                        ]);
                    }
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Photo not valid',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Please choose one photo',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function put(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idtestimony = $req['idtestimony'];
            $name = $req['name'];
            $job = $req['job'];
            $response = $req['response'];

            $data = [
                'name' => $name,
                'job' => $job,
                'response' => $response,
                'id' => $id
            ];

            $sql = TestimonyModel::Edit($data, $idtestimony);
            if ($sql) 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited testimony success',
                ]);
            } 
            else 
            {
                return json_encode([
                    'status' => 'success',
                    'message' => 'Edited testimony failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }

    }

    function remove(Request $req)
    {
        $id = Auth::id();
        if (!empty($id)) 
        {
            $idtestimony = $req['idtestimony'];
            if (true) 
            {
                //remove database
                $sql = TestimonyModel::Remove($idtestimony);
                if ($sql) 
                {
                    return json_encode([
                        'status' => 'success',
                        'message' => 'Delete testimony success',
                    ]);
                } 
                else 
                {
                    return json_encode([
                        'status' => 'error',
                        'message' => 'Delete testimony failed',
                    ]);
                }
            } 
            else 
            {
                return json_encode([
                    'status' => 'error',
                    'message' => 'Delete cover failed',
                ]);
            }
        } 
        else 
        {
            return json_encode([
                'status' => 'error',
                'message' => 'Access denied',
            ]);
        }
    }
}
