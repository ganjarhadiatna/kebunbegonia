<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BannerModel;
use App\ServiceModel;
use App\GaleryModel;
use App\ArticleModel;
use App\TestimonyModel;
use App\TagModel;
use App\NoteModel;

class WebController extends Controller
{
    protected $token = '2052574873.877ad90.fba27260465e4fe69cd359a0f2563274';

    function index()
    {
        $tag = 'uiux';
        $url = 'https://api.instagram.com/v1/users/self/media/recent?access_token='.$this->token.'&count=4';

        $banner = BannerModel::AllBanner(10);
        $service = ServiceModel::AllService(10);
        $note = NoteModel::AllNote(8);
        $galery = GaleryModel::AllGalery(4, 'desc');
        $article = ArticleModel::AllArticleBlog(3, 'desc');
        $pinned = ArticleModel::AllArticlePined(3, 'desc');
        $testimony = TestimonyModel::AllTestimony(3, 'desc');
        return view('web.index', [
            'path' => 'home',
            'title' => 'Tempat Wisata Kebun Bunga Dengan 500+ Spot Untuk Selfie dan Berfoto - Kebun Begonia Lembang',
            'banner' => $banner,
            'service' => $service,
            'note' => $note,
            'galery' => $galery,
            'article' => $article,
            'pinned' => $pinned,
            'testimony' => $testimony
        ]);

        //$insta = json_decode(file_get_contents($url));//$this->instagram_connect($url);
        //echo json_encode($this->instagram_access_token());
    }

    function search()
    {
        $ctr = $_GET['src'];
        $nav = $_GET['nav'];
        if ($nav == 'article') {
            $search = ArticleModel::SearchArticle($ctr, 10, 'desc');   
        }
        if ($nav == 'galery') {
            $search = GaleryModel::SearchGalery($ctr, 10, 'desc');   
        }
        return view('web.search.index', [
            'path' => 'search',
            'title' => 'Hasil Pencarian',
            'search' => $search
        ]);
    }

    function admin()
    {
        return view('admin.index', [
            'path' => 'home'
        ]);
    }

    function instagram()
    {
        $url = 'https://api.instagram.com/v1/users/self/media/recent?access_token='.$this->token.'&count=16';
        $insta = json_decode(file_get_contents($url));
        return view('web.instagram.list', [
            'path' => 'instagram',
            'insta' => $insta->data
        ]);
    }

    function instagram_access_token()
    {
        //CLIENT_ID:877ad909a58d49c98d8e008fd49fd9bc
        //https://api.instagram.com/oauth/authorize/?client_id=877ad909a58d49c98d8e008fd49fd9bc&redirect_uri=http://localhost:8000&response_type=code&scope=public_content

        $fields = array(
            'client_id'     => '877ad909a58d49c98d8e008fd49fd9bc',
            'client_secret' => 'a7544070697641b6ad6efa0421c34972',
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => 'http://localhost:8000/',
            'code'          => 'a883cf9e6e1041368c5af8a6d5f200bb'
        );
        $url = 'https://api.instagram.com/oauth/access_token';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_POST,true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch); 
        $result = json_decode($result);
        return $result;
    }

    function instagram_connect( $api_url )
    {
        $connection_c = curl_init(); // initializing
        curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
        curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
        curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
        $json_return = curl_exec( $connection_c ); // connect and get json data
        curl_close( $connection_c ); // close connection
        $result = json_decode( $json_return ); // decode and return
        return $result;
    }

    //sites
    function aboutUs()
    {
        return view('web.sites.aboutUs', [
            'path' => 'information',
            'title' => 'About Us - Kebun Begonia Lembang',
        ]);
    }
    function tnc()
    {
        return view('web.sites.tnc', [
            'path' => 'information',
            'title' => 'Terms & Condition - Kebun Begonia Lembang',
        ]);
    }
    function privacy()
    {
        return view('web.sites.privacy', [
            'path' => 'information',
            'title' => 'Privacy - Kebun Begonia Lembang',
        ]);
    }
    function faq()
    {
        return view('web.sites.faq', [
            'path' => 'information',
            'title' => 'FAQ - Kebun Begonia Lembang',
        ]);
    }
}
