<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NoteModel extends Model
{
    protected $table = 'note';

    //create
    function scopeInsert($quer, $data)
    {
        return DB::table($this->table)
        ->insert($data);
    }

    //update
    function scopeEdit($quer, $data, $idnote)
    {
        return DB::table($this->table)
        ->where('note.idnote', $idnote)
        ->update($data);
    }

    //delete
    function scopeRemove($quer, $idnote)
    {
        return DB::table($this->table)
        ->where('note.idnote', $idnote)
        ->delete();
    }

    //read
    function scopeGetnoteRecentId($query)
    {
        return DB::table($this->table)
        ->orderBy('note.idnote', 'desc')
        ->limit(1)
        ->value('note.idnote');
    }
    function scopenoteImageById($query, $idnote)
    {
        return DB::table($this->table)
        ->where('note.idnote', $idnote)
        ->value('note.cover');
    }
    function scopeDetailnote($query, $idnote)
    {
        return DB::table($this->table)
        ->select(
            'note.idnote',
            'note.cover',
            'note.icon',
            'note.title',
            'note.description',
            'note.date',
            'note.link',
            'users.id',
            'users.name',
            'users.username'
        )
        ->where('note.idnote', $idnote)
        ->join('users','users.id', '=', 'note.id')
        ->get();
    }
    function scopeAllnote($query, $limit, $order = 'asc')
    {
        return DB::table($this->table)
        ->select(
            'note.idnote',
            'note.cover',
            'note.icon',
            'note.title',
            'note.description',
            'note.date',
            'note.link',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'note.id')
        ->orderBy('note.idnote', $order)
        ->paginate($limit);
    }
    function scopeSearchnote($query, $ctr, $limit, $order = 'asc')
    {
        $searchValues = preg_split('/\s+/', $ctr, -1, PREG_SPLIT_NO_EMPTY);
        return DB::table($this->table)
        ->select(
            'note.idnote',
            'note.cover',
            'note.icon',
            'note.title',
            'note.description',
            'note.date',
            'note.link',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'note.id')
        ->where('note.title','like',"%$ctr%")
        ->orWhere('note.description','like',"%$ctr%")
        ->orWhere(function ($q) use ($searchValues)
        {
            foreach ($searchValues as $value) {
            	$q->orWhere('note.title','like',"%$value%");
                $q->orWhere('note.description','like',"%$value%");
            }
        })
        ->orderBy('note.idnote', $order)
        ->paginate($limit);
    }
    function scopenoteById($query, $idnote)
    {
        return DB::table($this->table)
        ->select(
            'note.idnote',
            'note.cover',
            'note.icon',
            'note.title',
            'note.description',
            'note.date',
            'note.link',
            'users.id',
            'users.name',
            'users.username'
        )
        ->join('users','users.id', '=', 'note.id')
        ->where('note.idnote', $idnote)
        ->get();
    }
}
