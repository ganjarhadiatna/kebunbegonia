@extends('layouts.admin')
@section('content')

<link href="{{ asset('/summernote/dist/summernote-lite.css') }}" rel="stylesheet">
<script src="{{ asset('/summernote/dist/summernote-lite.js') }}"></script>

<!--
<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
-->
<script>
    var server = '{{ url("/") }}';

    function publish() {
        var fd = new FormData();

        var ctn = 1;//$('#cover')[0].files.length;
        //var cover = $('#cover')[0].files[0];
        var idarticle = $('#idarticle').val();
        var title = $('#title').val();
        var content = $('#content').val();
        var service = $('#service').val();
        var status = $('input[name=status]:checked').val();

        if (ctn > 0) {
            fd.append('idarticle', idarticle);
            fd.append('title', title);
            fd.append('content', content);
            fd.append('service', service);
            fd.append('status', status);

            $.each($('#form-publish').serializeArray(), function(a, b) {
                fd.append(b.name, b.value);
            });

            $.ajax({
                url: '{{ route("article-put") }}',
                data: fd,
                processData: false,
                contentType: false,
                dataType: 'json',
                type: 'post',
                beforeSend: function() {
                    loadPopup('show');
                }
            })
            .done(function(data) {
                if (data.status == 'success') 
                {
                    window.location = '{{ route("article-index") }}';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                    //console.log(data);
                }
            })
            .fail(function(data) {
                //alert(data.responseJSON);
                loadPopup('hide');
                console.log(data.responseJSON);
            })
            .always(function () {
                //after done
            });
        } else {
            alert('Please select one cover');
        }

        return false;
    }
    $(document).ready(function () {
        $('#content').summernote({
            minHeight: 300,
            required: true
        });
    });
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Edit</p>
    <h1>Artikel & Blog</h1>
    <div class="bdr"></div>
</div>

<form 
    id="form-publish" 
    method="post" 
    action="javascript:void(0)" 
    enctype="multipart/form-data" 
    onsubmit="publish()">
    <div class="content-create">
        @foreach ($article as $at)
            
            <input type="hidden" id="idarticle" value="{{ $at->idarticle }}">

            <div class="cc-left">
                <div class="cc-block">
                    <div class="label">
                        Cover
                    </div>
                    <div class="desc">
                        Cover tidak bisa dirubah
                    </div>
                    <div 
                        class="image image-150px"
                        style="
                            background-image: url({{ asset('/img/article/thumbnails/'.$at->cover) }});
                        "></div>
                </div>
                <div class="cc-block">
                    <div class="label">
                        Judul
                    </div>
                    <div class="desc">
                        Gunakan kalimat yang jelas dan tidak terlalu panjang
                    </div>
                    <input 
                        type="text"
                        name="title"
                        id="title"
                        required="required"
                        class="txt txt-primary-color"
                        value="{{ $at->title }}"
                        placeholder="">
                </div>

                <div class="cc-block">
                    <div class="label">
                        Konten & Layanan (opsional)
                    </div>
                    <div class="desc">
                        Apakah artikel ini untuk "Konten & Layanan"?
                        jika tidak maka boleh tidak dipilih dan artikel
                        ini akan menjadi artikel biasa
                    </div>
                    <select name="service" id="service" class="slc slc-sekunder-color">
                        <option value="0">Pilih konten & layanan</option>
                        @foreach ($service as $sv)
                            @if ($sv->idservice == $at->idservice)
                                <option value="{{ $sv->idservice }}" selected="true">{{ $sv->title }}</option>
                            @else
                                <option value="{{ $sv->idservice }}">{{ $sv->title }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="cc-block">
                    <div class="label">
                        Konten Artikel
                    </div>
                    <div class="desc">
                        Berisi konten dari artikel ataupun blog yang akan dibuat
                    </div>
                    <textarea 
                        name="content" 
                        id="content"
                        required="required"
                        class="txt txt-primary-color edit-text">{{ $at->content }}</textarea>
                </div>
            </div>

            <div class="cc-right">
                <div class="cc-block bdr-all">
                    <div class="label">
                        Catatan
                    </div>
                    <ul class="cc-note">
                        <li>Artikel wajib menggunakan cover yaitu gambar</li>
                        <li>Judul dan konten tidak boleh dikosongkan</li>
                        <li>
                            Versi text editor ini belum bisa mengupload gambar
                            dari device, jadi jangan dulu memasukan gambar
                            kecuali gambar dari internet.
                        </li>
                    </ul>
                </div>

                <div class="cc-block bdr-all">
                    <div class="label">
                        Status Artikel
                    </div>
                    <div class="padding-bottom-10px">
                        <ul class="cc-note">
                            <li>
                                Jika ingin menyimpan artikel ke dalam "Draft" terlebih dahulu, pilih "Draft".
                            </li>
                            <li>
                                Jika ingin langsung mempublish langsung artikel, pilih "Publish".
                            </li>
                        </ul>
                    </div>
                    <div class="rd" id="status">
                        @if ($at->is_draft == '1')
                            <input type="radio" name="status" value="1" checked>
                            <label for="status">Draft</label>
                            <br>
                            <input type="radio" name="status" value="0">
                            <label for="status">Publish</label>
                        @else
                            <input type="radio" name="status" value="1">
                            <label for="status">Draft</label>
                            <br>
                            <input type="radio" name="status" value="0" checked>
                            <label for="status">Publish</label>
                        @endif
                    </div>
                </div>

                <div class="cc-block">
                    <input 
                        type="submit" 
                        value="Simpan"
                        class="btn btn-main-color">
                </div>

                <div class="cc-block">
                    <input 
                        type="button" 
                        value="Cancel"
                        onclick="goBack()" 
                        class="btn btn-sekunder-color">
                </div>

            </div>
        @endforeach
        <div class="padding-10px"></div>
    </div>
</form>

@endsection