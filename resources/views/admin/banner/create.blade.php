@extends('layouts.admin')
@section('content')

<script>
    var server = '{{ url("/") }}';

    function publish() {
		var fd = new FormData();

        var ctn = $('#cover')[0].files.length;
        var cover = $('#cover')[0].files[0];
		var title = $('#title').val();
        var description = $('#description').val();
        var link = $('#link').val();
        var position = $('#position').val();

        if (ctn > 0) {
            fd.append('cover', cover);
            fd.append('title', title);
            fd.append('description', description);
            fd.append('link', link);
            fd.append('position', position);

            $.each($('#form-publish').serializeArray(), function(a, b) {
			   	fd.append(b.name, b.value);
			});

            $.ajax({
                url: '{{ route("banner-publish") }}',
				data: fd,
				processData: false,
				contentType: false,
                dataType: 'json',
				type: 'post',
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = '{{ route("banner-index") }}';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                }
			})
			.fail(function(data) {
                loadPopup('hide');
			   	console.log(data.responseJSON);
			})
			.always(function () {
				//after done
			});
        } else {
            alert('Please select one cover');
        }

		return false;
	}
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Buat</p>
    <h1>Banner</h1>
    <div class="bdr"></div>
</div>

<form 
    id="form-publish" 
    method="post" 
    action="javascript:void(0)" 
    enctype="multipart/form-data" 
    onsubmit="publish()">
    <div class="content-create">
        <div class="cc-left">
            <div class="cc-block">
                <div class="label">
                    Cover
                </div>
                <div class="grid">
                    <div class="col-1 padding-bottom-10px">
                        <div
                            id="add-pict" 
                            class="image image-full"
                            style="
                                margin: auto;
                                "></div>
                    </div>
                    <div class="col-1 padding-top-20px">
                        <div class="position middle">
                            <input type="file" name="cover" id="cover" required="required">
                        </div>
                    </div>
                </div>
            </div>
            <div class="cc-block">
                <div class="label">
                    Judul (opsional)
                </div>
                <div class="desc">
                    Gunakan kalimat yang jelas dan tidak terlalu panjang
                </div>
                <input 
                    type="text"
                    name="title"
                    class="txt txt-primary-color"
                    id="title"
                    placeholder="">
            </div>
            <div class="cc-block">
                <div class="label">
                    Deskripsi (opsional)
                </div>
                <div class="desc">
                    Jelaskan tentang banner yang diupload, gunakan kalimat yang jelas,
                    tidak berbelit-belit dan langsung ke poko bahasan.
                </div>
                <div class="padding-bottom-5px">
                    <p class="ctn-main-font ctn-14px ctn-sek-color">
                        <span id="desc-length">0</span>/250
                    </p>
                </div>
                <textarea 
                    name="description" 
                    id="description"
                    class="txt txt-primary-color edit-text"
                    maxlength="250"></textarea>
            </div>
            <div class="cc-block">
                <div class="label">
                    Link (opsional)
                </div>
                <div class="desc">
                    Bisa link artikel yang sudah dibuat ataupun link lainnya.
                </div>
                <input 
                    type="link"
                    name="link"
                    class="txt txt-primary-color"
                    id="link"
                    placeholder="">
            </div>
            <div class="cc-block">
                <div class="label">
                    Posisi Text
                </div>
                <div class="desc">
                    <p>
                        Posisi dari judul, deskripsi dan link banner.
                    </p>
                </div>
                <select class="slc" style="width: 150px;" name="position" id="position">
                    <option value="center" selected="true">Tengah</option>
                    <option value="bottom">Bawah</option>
                    <option value="disable">Sembunyikan</option>
                </select>
            </div>
        </div>
        <div class="cc-right">
            <div class="cc-block bdr-all">
                <div class="label">
                    Catatan
                </div>
                <ul class="cc-note">
                    <li>Banner wajib menggunakan gambar</li>
                    <li>Jika judul, deskripsi dan link dikosongkan maka yang dimunculkan hanya gambar saja</li>
                    <li>Judul, deskripsi dan link bertipe opsional artinya boleh diisi boleh juga tidak</li>
                </ul>
            </div>
            <div class="cc-block">
                <input 
                    type="submit" 
                    value="Simpan"
                    class="btn btn-main-color">
            </div>
            <div class="cc-block">
                <input 
                    type="button" 
                    value="Cancel"
                    onclick="goBack()" 
                    class="btn btn-sekunder-color">
            </div>
        </div>
    </div>
</form>

@endsection