@extends('layouts.admin')
@section('content')
<script>
    var server = '{{ url("/") }}';

    function publish() {
		var fd = new FormData();

        var ctn = 1;
        var idnote = $('#idnote').val();
        var icon = $('#icon').val();
        var title = $('#title').val();
        var description = $('#description').val();
        var link = $('#link').val();

        if (ctn > 0) {
            fd.append('idnote', idnote);
            fd.append('icon', icon);
            fd.append('title', title);
            fd.append('description', description);
            fd.append('link', link);

            $.each($('#form-publish').serializeArray(), function(a, b) {
			   	fd.append(b.name, b.value);
			});

            $.ajax({
                url: '{{ route("note-put") }}',
				data: fd,
				processData: false,
				contentType: false,
                dataType: 'json',
				type: 'post',
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = '{{ route("note-index") }}';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                }
			})
			.fail(function(data) {
                loadPopup('hide');
			   	alert(data.responseJSON.message);
			})
			.always(function () {
				//loadPopup('hide');
			});
        } else {
            alert('Please select one cover');
        }

		return false;
	}
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Edit</p>
    <h1>Catatan situs</h1>
    <div class="bdr"></div>
</div>

<form 
    id="form-publish" 
    method="post" 
    action="javascript:void(0)" 
    enctype="multipart/form-data" 
    onsubmit="publish()">
    <div class="content-create">
        <div class="cc-left">
        @foreach ($note as $gl)
            <input type="hidden" id="idnote" value="{{ $gl->idnote }}">

            <div class="cc-block">
                <div class="label">
                    Cover
                </div>
                <div class="desc">
                    <p>Gambar tidak bisa dirubah</p>
                </div>
                <div 
                    class="image image-150px"
                    style="background-image: url({{ asset('/img/note/thumbnails/'.$gl->cover) }});"></div>
            </div>

            <div class="cc-block">
                <div class="label">
                    Ikon
                </div>
                <div class="desc">
                    <p>Contoh: "fa fa-lg fa-heart"</p>
                </div>
                <input 
                    type="text"
                    name="icon"
                    id="icon"
                    class="txt txt-primary-color"
                    value="{{ $gl->icon }}"
                    required="required"
                    placeholder="fa fa-lg fa-nama-icon">
            </div>

            <div class="cc-block">
                <div class="label">
                    Judul
                </div>
                <div class="desc">
                    Gunakan kalimat yang jelas dan tidak terlalu panjang
                </div>
                <input 
                    type="text"
                    name="title"
                    id="title"
                    required="required"
                    class="txt txt-primary-color"
                    value="{{ $gl->title }}" 
                    placeholder="">
            </div>

            <div class="cc-block">
                <div class="label">
                    Deskripsi (opsional)
                </div>
                <div class="desc">
                    Jelaskan tentang galeri yang diupload, gunakan kalimat yang jelas,
                    tidak berbelit-belit dan langsung ke poko bahasan.
                </div>
                <textarea 
                    name="deskripsi" 
                    id="description"
                    required="required"
                    class="txt txt-primary-color edit-text">{{ $gl->description }}</textarea>
            </div>

            <div class="cc-block">
                <div class="label">
                    Link (opsional)
                </div>
                <input 
                    type="link"
                    name="link"
                    id="link"
                    class="txt txt-primary-color"
                    value="{{ $gl->link }}" 
                    placeholder="https://">
            </div>
        @endforeach
        </div>
        <div class="cc-right">
            <div class="cc-block bdr-all">
                <div class="label">
                    Catatan
                </div>
                <ul class="cc-note">
                    <li>Jika deskripsi dikosongkan maka yang dimunculkan hanya gambar saja</li>
                    <li>Deskripsi bertipe opsional artinya boleh diisi boleh juga tidak</li>
                </ul>
            </div>
            <div class="cc-block">
                <input 
                    type="submit" 
                    value="Simpan"
                    class="btn btn-main-color">
            </div>
            <div class="cc-block">
                <input 
                    type="button" 
                    value="Cancel"
                    onclick="goBack()" 
                    class="btn btn-sekunder-color">
            </div>
        </div>
    </div>
</form>

@endsection