@extends('layouts.admin')
@section('content')
<?php 
    use App\TagModel; 
?>
<script>
    var server = '{{ url("/") }}';
    
    function deletenote ($idnote) 
    { 
        var a = confirm('Delete this note?');
        if (a == true) {
            $.ajax({
                url: '{{ route("note-remove") }}',
                dataType: 'json',
                type: 'post',
                data: {
                    'idnote': $idnote
                },
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = '{{ route("note-index") }}';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                }
			})
			.fail(function(data) {
                loadPopup('hide');
                alert(data.responseJSON.message);
			   	//console.log(data.responseJSON);
			})
			.always(function () {
				//after done
			});
        }
    }
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Pengelolaan</p>
    <h1>Catatan situs</h1>
    <div class="bdr"></div>
    <div class="padding-10px"></div>
    <p>
        Masukan catatan situs sperti harga tiket, alamat, pemesanan dsb.
    </p>
</div>

<div class="content-page">
    <div class="cp-top">
        <div class="cp-left">
            <a href="{{ route('note-create') }}">
                <button class="btn btn-main-color btn-radius">
                    <span class="fa fa-lg fa-plus"></span>
                    <span>Tambahkan Catatan</span>
                </button>
            </a>
        </div>
        <div class="cp-right">
            <form action="#">
                <div class="search">
                    <input 
                        type="text" 
                        class="src txt txt-main-color" 
                        placeholder="Search..." 
                        required="required">
                    <button class="bt btn btn-main-color" type="submit">
                        <span class="fa fa-lg fa-search"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="cp-mid">
        <table>
            <thead>
                <tr>
                    <th width="20">No</th>
                    <th width="100">Gambar</th>
                    <th width="40">Ikon</th>
                    <th width="200" class="mobile">Judul</th>
                    <th class="mobile">Deskripsi</th>
                    <th width="100" class="mobile">Link</th>
                    <th width="100" class="mobile">Tanggal</th>
                    <th width="100"></th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                @foreach ($note as $gl)
                <tr>
                    <td><strong>{{ $i }}</strong></td>
                    <td>
                        <div 
                            class="image image-100px" 
                            style="
                                background-image: url({{ asset('/img/note/thumbnails/'.$gl->cover) }});
                            "></div>
                    </td>
                    <td class="mobile">
                        <span class="txt-site txt-12 {{ $gl->icon }}"></span>
                    </td>
                    <td class="mobile">{{ $gl->title }}</td>
                    <td class="mobile">{{ $gl->description }}</td>
                    <td class="mobile">{{ $gl->link }}</td>
                    <td class="mobile">{{ $gl->date }}</td>
                    <td>
                        <a href="{{ url('/admin/note/edit/'.$gl->idnote) }}">
                            <button class="btn btn-sekunder-color btn-circle">
                                <span class="fa fa-1x fa-pencil-alt"></span>
                            </button>
                        </a>
                        <button 
                            class="btn btn-sekunder-color btn-circle"
                            onclick="deletenote('{{ $gl->idnote }}')">
                            <span class="fa fa-1x fa-trash-alt"></span>
                        </button>
                    </td>
                </tr>
                <?php $i++ ?>
                @endforeach
            </tbody>
        </table>
        <div>
            {{ $note->links() }}
        </div>
    </div>
</div>
@endsection