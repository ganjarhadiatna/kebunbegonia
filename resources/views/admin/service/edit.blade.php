@extends('layouts.admin')
@section('content')
<script>
    var server = '{{ url("/") }}';

    function publish() {
		var fd = new FormData();

        var idservice = $('#idservice').val();
        var icon = $('#icon').val();
        var title = $('#title').val();
        var link = $('#link').val();
        var description = $('#description').val();
		var ctn = 1;

        if (ctn > 0) {
            fd.append('idservice', idservice);
            fd.append('icon', icon);
            fd.append('title', title);
            fd.append('link', link);
            fd.append('description', description);

            $.each($('#form-publish').serializeArray(), function(a, b) {
			   	fd.append(b.name, b.value);
			});

            $.ajax({
                url: '{{ route("service-put") }}',
				data: fd,
				processData: false,
				contentType: false,
                dataType: 'json',
				type: 'post',
				beforeSend: function() {
					loadPopup('show');
				}
			})
			.done(function(data) {
			   	if (data.status == 'success') 
                {
                    window.location = '{{ route("service-index") }}';
                } 
                else 
                {
                    loadPopup('hide');
                    alert(data.message);
                    
                }
			})
			.fail(function(data) {
                loadPopup('hide');
			   	alert(data.responseJSON.message);
			})
			.always(function () {
				//after done
			});
        } else {
            alert('Please set all field');
        }

		return false;
	}
</script>

<div class="padding-top-20px"></div>

<div class="title-page">
    <p>Edit</p>
    <h1>Konten & Layanan</h1>
    <div class="bdr"></div>
</div>

<form 
    id="form-publish" 
    method="post" 
    action="javascript:void(0)" 
    enctype="multipart/form-data" 
    onsubmit="publish()">
    <div class="content-create">
        <div class="cc-left">
        @foreach ($service as $sv)
            <input type="hidden" id="idservice" value="{{ $sv->idservice }}">
            <div class="cc-block">
                <div class="label">
                    Ikon
                </div>
                <div class="desc">
                    <p>Contoh: "fa fa-lg fa-heart"</p>
                </div>
                <input 
                    type="text"
                    name="icon"
                    id="icon"
                    class="txt txt-primary-color"
                    value="{{ $sv->icon }}"
                    required="required"
                    placeholder="fa fa-lg fa-nama-icon">
            </div>
            <div class="cc-block">
                <div class="label">
                    Judul
                </div>
                <div class="desc">
                    <p>Gunakan kalimat yang jelas dan tidak terlalu panjang</p>
                </div>
                <input 
                    type="text"
                    name="title"
                    id="title"
                    class="txt txt-primary-color"
                    value="{{ $sv->title }}"
                    required="required"
                    placeholder="">
            </div>
            <div class="cc-block">
                <div class="label">
                    Deskripsi
                </div>
                <div class="desc">
                    <p>
                        Jelaskan tentang service yang ditambahakan, gunakan kalimat yang jelas,
                        tidak berbelit-belit dan langsung ke poko bahasan.
                    </p>
                </div>
                <textarea 
                    name="deskripsi" 
                    id="description"
                    class="txt txt-primary-color edit-text">{{ $sv->description }}</textarea>
            </div>
            <div class="cc-block">
                <div class="label">
                    Link
                </div>
                <div class="desc">
                    <p>Link bisa berupa artikel yang telah dibuat</p>
                </div>
                <input 
                    type="link"
                    name="link"
                    id="link"
                    class="txt txt-primary-color"
                    value="{{ $sv->link }}"
                    placeholder="">
            </div>
        @endforeach
        </div>
        <div class="cc-right">
            <div class="cc-block bdr-all">
                <div class="label">
                    Catatan
                </div>
                <ul class="cc-note">
                    <li>
                        Ikon disini berupa kode icon yang bisa didapatkan di
                        <a href="https://fontawesome.com/icons?d=gallery" target="_blank">
                            link ini
                        </a>
                    </li>
                    <li>Judul, deskripsi dan link bertipe opsional artinya boleh diisi boleh juga tidak</li>
                </ul>
            </div>
            <div class="cc-block">
                <input 
                    type="submit" 
                    value="Simpan"
                    class="btn btn-main-color">
            </div>
            <div class="cc-block">
                <input 
                    type="button" 
                    value="Cancel"
                    onclick="goBack()" 
                    class="btn btn-sekunder-color">
            </div>
        </div>
    </div>
</form>

@endsection