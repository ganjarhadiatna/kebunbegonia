<!DOCTYPE html>
<html>
<head>
    <title>Admin Request Reset Password</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--css-->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('icons/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('scss/app.css') }}" />

    <!--js-->
    <script src="{{ asset('js/jquery.js') }}"></script>

</head>
<body>
    <div class="frm-sign">
        <div class="fs-top">
            <a href="{{ url('/') }}">
                <img 
                    src="{{ asset('img/sites/logo-center.png') }}" 
                    alt="Kebun Begonia Glory"
                    class="logo">
            </a>
        </div>
        <div class="fs-mid">
            
            @if(session()->has('login_error'))
                <div class="fs-text">
                    <div class="alert alert-success">
                        {{ session()->get('login_error') }}
                    </div>
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                @csrf
                <div class="fs-block">
                    <div class="fs-left">
                        <div class="icn fa fa-lg fa-envelope"></div>
                    </div>
                    <div class="fs-right">
                        <input 
                            type="email" 
                            name="email" 
                            id="email" 
                            class="txt txt-main-color"
                            placeholder="E-mail address"
                            value="{{ old('email') }}"
                            required="required">
                    </div>
                </div>
                @if ($errors->has('email'))
                    <div class="padding-top-10px">
                        <div class="alert alert-error">
                            {{ $errors->first('email') }}
                        </div>
                    </div>
                @endif

                <div class="padding-10px"></div>

                <div class="fs-button">
                    <input type="submit" value="Send Password Reset Link" class="btn btn-main-color">
                </div>
            </form>

        </div>
    </div>
</body>
</html>