<!DOCTYPE html>
<html>
<head>
    <title>Admin Reset Password</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--css-->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('icons/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('scss/app.css') }}" />

    <!--js-->
    <script src="{{ asset('js/jquery.js') }}"></script>

</head>
<body>
    <div class="frm-sign">
        <div class="fs-top">
            <a href="{{ url('/') }}">
                <img 
                    src="{{ asset('img/sites/logo-center.png') }}" 
                    alt="Kebun Begonia Glory"
                    class="logo">
            </a>
        </div>
        <div class="fs-mid">
            <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
                @csrf
    
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="fs-block">
                    <div class="fs-left">
                        <div class="icn fa fa-lg fa-envelope"></div>
                    </div>
                    <div class="fs-right">
                        <input 
                            type="text" 
                            name="email" 
                            id="email" 
                            class="txt txt-main-color"
                            placeholder="E-mail address"
                            value="{{ old('email') }}"
                            required="required"
                            autofocus>
                    </div>
                </div>
                @if ($errors->has('email'))
                <div class="padding-top-10px">
                    <div class="alert alert-error">
                        {{ $errors->first('email') }}
                    </div>
                </div>
                @endif

                <div class="padding-10px"></div>

                <div class="fs-block">
                    <div class="fs-left">
                        <div class="icn fa fa-lg fa-key"></div>
                    </div>
                    <div class="fs-right">
                        <input 
                            type="password" 
                            name="password" 
                            id="password" 
                            class="txt txt-main-color"
                            required="required"
                            placeholder="New password">
                    </div>
                </div>
                @if ($errors->has('password'))
                <div class="padding-top-10px">
                    <div class="alert alert-error">
                        {{ $errors->first('password') }}
                    </div>
                </div>
                @endif

                <div class="padding-10px"></div>

                <div class="fs-block">
                    <div class="fs-left">
                        <div class="icn fa fa-lg fa-key"></div>
                    </div>
                    <div class="fs-right">
                        <input 
                            id="password-confirm" 
                            type="password" 
                            class="txt txt-main-color" 
                            name="password_confirmation" 
                            placeholder="Confirm password"
                            required>
                    </div>
                </div>

                <div class="padding-10px"></div>

                <div class="fs-button">
                    <input type="submit" value="Reset Password" class="btn btn-main-color">
                </div>

            </form>
        </div>
    </div>
</body>
</html>
