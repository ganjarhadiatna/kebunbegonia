<?php 
    use App\ServiceModel; 
    use App\ArticleModel;
?>
<?php
    if (isset($path)) {
        $nowPath = '#'.$path;
    } else {
        $nowPath = '';
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    
    <!--meta-->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="google-site-verification" content="7gjYBIjkR1XNmK_TnKMlOb37ukFqbMSPlBA8r_wHHD4" />

    <meta name = "format-detection" content = "telephone=no" />
    <meta name="description" content="Kebun Begonia Lembang adalah salah satu tempat 
    wisata yang menyuguhkan aneka tanaman hias yang beraneka ragam dengan bunga 
    BEGONIA yang menjadi ikon utamanya. Selain menyuguhkan tanaman hias dan udara 
    lembang yang sejuk, Kebun Begonia Lembang juga memiliki berbagai fasilitas 
    sperti: Kebun Bunga, Kebun Sayur, Cafe/Restoran, Toko Aksesoris, Pelatihan 
    dan Reservasi Tempat." />
    <meta name="keywords" content="Kebun Bunga, Kebun Bunga Glory, Kebun Bunga Begonia, 
    Kebun Bunga Begonia Lembang, Kebun Begonia, Kebun Begonia Lembang, Wisata Bunga, 
    Tempat Wisata di Lembang, Wisata di Lembang, Wisata di Bandung, Spot Selfie, Spot Berfoto" />
    <meta name="generator" content="KebunBegoniaLembang" />
    <meta name="robots" content="index,follow" />

    <link rel="shortcut icon" href="{{ asset('/img/sites/favicon.ico') }}">

    <!--css-->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('icons/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('scss/app.css') }}" />

    <!--js-->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery.bxslider.min.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127323790-1"></script>
    <script type="text/javascript"> //<![CDATA[ 
    var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
    document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
    //]]>
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-127323790-1');

        function showSubMenu(nav) {
            var tr = $('#'+nav).attr('class');
            if (tr == 'sb-menu') {
                $('#'+nav).addClass('sb-open');
                //$('#'+nav).hide();
                //$('#'+nav).css('display', 'hide');
            } else {
                $('#'+nav).removeClass('sb-open');
                //$('#'+nav).show();
                //$('#'+nav).css('display', 'block');
            }
        }
        function opBar(stt) {  
            if (stt == 'show') {
                //$('#opBar').animate({right: 0}, 500);
                $('#opBar').addClass('pl-active');
            } else {
                //$('#opBar').animate({right: -350}, 500);
                $('#opBar').removeClass('pl-active');
            }
        }
        function opSearch(stt) {
            if (stt == 'show') {
                $('#search-popup').fadeIn();
                $('#src').focus();
            } else {
                $('#search-popup').fadeOut();
            }
        }
        function toLeft(path) {
			var wd = $('#'+path).width();
			var sc = $('#'+path).scrollLeft();
			if (sc >= 0) {
				$('#'+path).animate({scrollLeft: (sc - wd)}, 500);
			}
		}
		function toRight(path) {
			var wd = $('#'+path).width();
			var sc = $('#'+path).scrollLeft();
			if (true) {
				$('#'+path).animate({scrollLeft: (sc + wd)}, 500);
			}
		}
        function opMap(stt) {  
            var data = '\
                <iframe \
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2801.231154216802!2d107.63726792387176!3d-6.825771055646161!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e0f367d63315%3A0xac823530fc733ca6!2sKebun+Begonia!5e0!3m2!1sid!2sid!4v1535613699402" \
                    width="1000" \
                    height="500" \
                    frameborder="0" \
                    style="border:0" \
                    allowfullscreen></iframe>\
                ';
            if (stt == 'show') {
                $('#map-popup').fadeIn();
                $('#placeMap').html(data);
            } else {
                $('#map-popup').fadeOut();
                $('#placeMap').html('');
            }
        }
        $(document).ready(function () {
            $('#toTop').on('click', function () {
                $('body, html').animate({scrollTop: 0}, 500);
            });

            $('{{ $nowPath }}').addClass('active');
            
            $(window).scroll(function(event) {
                var top = $(this).scrollTop();
                var hg = 300;
                if (top > hg) {
                    $('#plToTop').fadeIn();
                } else {
                    $('#plToTop').fadeOut();
                }
            });
        });
    </script>

</head>
<body>
    <div class="header">
        <div class="place" id="pl-header">
            <div class="menu">
                
                <div class="col col-1">
                    <div class="pl-logo">
                        <a href="{{ url('/') }}">
                            <img 
                                src="{{ asset('img/sites/logo.png') }}" 
                                class="logo">
                        </a>
                    </div>
                </div>

                <div class="col col-2">
                    <div class="pl-bar">
                        <ul class="mn-menu">
                            <li class="mn-list mn-bar" id="bar" onclick="opBar('show')">
                                <span class="fa fa-lg fa-bars"></span>
                            </li>
                        </ul>
                    </div>
                    <div class="pl-menu" id="opBar">
                        <ul class="mn-menu nav">
                            <li class="mn-list mn-close">
                                <span class="icn fa fa-lg fa-times" onclick="opBar('hide')"></span>
                            </li>
                        </ul>

                        <ul class="mn-menu">
                            <li class="mn-list" id="home">
                                <a class="mn-link" href="{{ url('/') }}">
                                    Beranda
                                </a>
                            </li>
                            <li class="mn-list" id="service">
                                <a class="mn-link" href="javascript:void(0)" onclick="showSubMenu('head-service')">
                                    <span>Konten & Layanan</span>
                                    <span class="fa fa-lg fa-angle-down"></span>
                                </a>
                                <ul class="sb-menu" id="head-service">
                                    @foreach (ServiceModel::AllService(10) as $sv)
                                        <li class="sb-list">
                                            <a class="sb-link" href="{{ url('/service/'.strtolower(urlencode($sv->title))) }}">
                                                <span class="icn fa fa-lg {{ $sv->icon }}"></span>
                                                <span class="ttl">{{ $sv->title }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="mn-list" id="article">
                                <a class="mn-link" href="{{ url('/articles') }}">
                                    Artikel
                                </a>
                            </li>
                            <li class="mn-list" id="galery">
                                <a class="mn-link" href="{{ url('/galeries') }}">
                                    Galeri
                                </a>
                            </li>
                            <li class="mn-list" id="information">
                                <a class="mn-link" href="javascript:void(0)" onclick="showSubMenu('head-info')">
                                    <span>Informasi</span>
                                    <span class="fa fa-lg fa-angle-down"></span>
                                </a>
                                <ul class="sb-menu" id="head-info">
                                    <li class="sb-list">
                                        <a class="sb-link" href="{{ url('/sites/about-us') }}">
                                            <span class="icn far fa-lw fa-circle"></span>
                                            About Us
                                        </a>
                                    </li>
                                    <li class="sb-list">
                                        <a class="sb-link" href="{{ url('/sites/terms-n-conditions') }}">
                                            <span class="icn far fa-lw fa-circle"></span>
                                            Terms & Conditions
                                        </a>
                                    </li>
                                    <li class="sb-list">
                                        <a class="sb-link" href="{{ url('/sites/privacy') }}">
                                            <span class="icn far fa-lw fa-circle"></span>
                                            Privacy
                                        </a>
                                    </li>
                                    <li class="sb-list">
                                        <a class="sb-link" href="{{ url('/sites/faq') }}">
                                            <span class="icn far fa-lw fa-circle"></span>
                                            FAQ
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mn-list mn-search" id="search" onclick="opSearch('show')">
                                <span class="fa fa-lg fa-search"></span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="frm-popup" id="map-popup">
        <div class="fp-place">
            <div class="close">
                <button 
                    class="btn btn-main-color btn-radius" 
                    onclick="opMap('hide')">
                    <span class="fa fa-lg fa-times"></span>
                    <span>Tutup</span>
                </button>
            </div>
            <div class="fp-mid">
                <div class="map" id="placeMap"></div>
            </div>
        </div>
    </div>

    <div class="frm-popup" id="search-popup">
        <div class="fp-place">
            <div class="close">
                <button 
                    class="btn btn-main-color btn-radius" 
                    onclick="opSearch('hide')">
                    <span class="fa fa-lg fa-times"></span>
                    <span>Tutup</span>
                </button>
            </div>
            <div class="fp-mid">
                <div class="search">
                    <h2 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color" 
                        style="
                            text-align: center;
                            color: #fff;
                            padding-bottom: 10px;
                            font-weight: 500;
                        ">
                        Cari artikel atau gambar
                    </h2>
                    <form action="{{ url('/search') }}" method="get">
                        <input type="hidden" name="nav" value="article">
                        <input 
                            type="text" 
                            name="src" 
                            id="src" 
                            placeholder="Tekan enter" 
                            required="required">
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('web.main.image-popup')

    <div class="pin" id="plToTop" style="display: none;">
        <div class="ctn" id="toTop">
            <div class="icn fa fa-lg fa-arrow-up"></div>
        </div>
    </div>

    <div class="body">
        @yield('content')
    </div>
    
    <div class="footer">
        <div class="f-main split-3">
            <div class="place">
                <div class="col">
                    <div class="title">
                        <h3 class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
                            Akun sosial media
                        </h3>
                    </div>
                    <ul class="menu">
                        <li>
                            <a href="https://www.instagram.com/kebunbegonia/" target="_blank">
                                <div class="icn fab fa-lg fa-instagram"></div>
                                <div class="ttl">Instagram</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/pages/Kebun-Begonia-Lembang/710603522325762" target="_blank">
                                <div class="icn fab fa-lg fa-facebook"></div>
                                <div class="ttl">Facebook</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/KebunBegonia" target="_blank">
                                <div class="icn fab fa-lg fa-twitter"></div>
                                <div class="ttl">Twitter</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCSoqhNAg0ffsPqB0uUPbAFQ" target="_blank">
                                <div class="icn fab fa-lg fa-youtube"></div>
                                <div class="ttl">Youtube</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.tripadvisor.co.id/Attraction_Review-g7187358-d8857593-Reviews-Begonia_Garden-Lembang_West_Java_Java.html" target="_blank">
                                <div class="icn fab fa-lg fa-tripadvisor"></div>
                                <div class="ttl">Tripadvisor</div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="place">
                <div class="col">
                    <div class="title">
                        <h3 class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
                            Kontak & reservasi
                        </h3>
                    </div>
                    <ul class="menu">
                        <li>
                            <div class="icn fa fa-lw fa-phone"></div>
                            <div class="ttl">022 2788-527</div>
                        </li>
                        <li>
                            <div class="icn fa fa-lw fa-phone"></div>
                            <div class="ttl">081-2220-0202</div>
                        </li>
                    </ul>
                </div>
                <div class="col">
                    <div class="title">
                        <h3 class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
                            Email & Layanan
                        </h3>
                    </div>
                    <ul class="menu">
                        <li>
                            <div class="icn fa fa-lw fa-envelope"></div>
                            <div class="ttl">
                                admin@kebunbegonialembang.com
                            </div>
                        </li>
                        <li>
                            <div class="icn fa fa-lw fa-envelope"></div>
                            <div class="ttl">
                                info@kebunbegonialembang.com
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="place">
                <div class="col">
                    <div class="title">
                        <h3 class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
                            Alamat pada g-map
                        </h3>
                    </div>
                    <div class="padding-5px"></div>
                    <div class="map" id="map">
                        <div 
                            class="image image-full"
                            style="
                                background-image: url( {{ asset('/img/content/map.png') }});
                                cursor: pointer;
                            "
                            onclick="opMap('show')"
                        ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="copy">
            <div>
                Designed and powered by
                <strong>CocaCode</strong> --
                All Rights Reserved |
                <span class="fa fa-1x fa-copyright"></span>
                2018
                Kebun Begonia Glory
            </div>
            <div>
                <ul class="menu">
                    <li>
                        <a href="https://instagram.com/ganjar_hadiatna" target="_blank">
                            <span class="icn fab fa-lg fa-instagram"></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://pinterest.com/ganjar_hadiatna/my-portofolio" target="_blank">
                            <span class="icn fab fa-lg fa-pinterest"></span>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/ganjarhadiatna" target="_blank">
                            <span class="icn fab fa-lg fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        
                    </li>
                </ul>
            </div>
            <div>
                <script language="JavaScript" type="text/javascript">
                TrustLogo("https://kebunbegonialembang.com/public/img/sites/comodo_secure_seal_113x59_transp.png", "CL1", "none");
                </script>
                <!--<a  href="https://ssl.comodo.com" id="comodoTL">Comodo SSL</a>-->
            </div>
        </div>

    </div>
</body>
</html>