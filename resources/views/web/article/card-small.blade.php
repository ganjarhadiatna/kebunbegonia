<div 
	class="frm-today image" 
	style="background-image: url({{ asset('/img/article/thumbnails/'.$at->cover) }}">
    <div class="ft-cover">
        <div class="ft-place">
            <div class="ft-top">
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-20pt ctn-white-color">
                	{{ $at->title }}
                </h1>
            </div>
            <a href="{{ url('/article/'.base64_encode($at->idarticle)) }}">
            	<div class="ft-mid fa fa-lg fa-angle-right"></div>
            </a>
        </div>
    </div>
</div>