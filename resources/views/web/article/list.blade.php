@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="body-block">
    <div class="top">
        <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
            Artikel
        </p>
        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
            Berita & event terbaru
        </h1>
        <div class="bdr"></div>
    </div>
    @if (count($article) != 0)
        <div class="mid">
                
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @foreach ($article as $at)
                        @include('web.article.card')
                    @endforeach
                </div>
            </div>

        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $article->links() }}
            </div>
        </div>
    @else
        <div class="top">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                artikel masih kosong
            </h1>
        </div>
    @endif
</div>

@endsection