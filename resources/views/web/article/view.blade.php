@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

@foreach ($article as $at)
    <div 
        class="about-us au-image au-image-scroll"
        style="background-image: url({{ asset('/img/article/covers/'.$at->cover) }}">
    </div>
@endforeach

<div class="pl-article">
    <div class="pl-a-col-1">
    @foreach ($article as $at)
        <div class="frm-story">
            <div class="mid">
                <div class="title">
                    <h1 class="ctn-main-font ctn-small ctn-sek-color ctn-font-2">{{ $at->title }}</h1>
                </div>
                <div class="date padding-10px">
                    <span class="icn fa fa-lg fa-clock"></span>
                    <span>{{ date_format(date_create($at->date), "M d, Y") }}</span>
                    <span class="icn fa fa-lg fa-user"></span>
                    <span>{{ $at->name }}</span>
                </div>
                <div class="desc">
                    <?php echo $at->content; ?>
                </div>
            </div>
            <div class="paddign-10px"></div>
            <div class="bot padding-20px">
                <div id="disqus_thread"></div>
                <script>

                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://kebunbegonia.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
                </script>
                <noscript>
                    Please enable JavaScript to view the 
                    <a href="https://disqus.com/?ref_noscript">
                        comments powered by Disqus.
                    </a>
                </noscript>
            </div>
        </div>
    @endforeach
    </div>
</div>

@if (count($newArticle) > 0)
<div class="bdr-bottom"></div>

<div class="body-block">
    <div class="top">
        <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                Artikel
        </p>
        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                Berita & event lainnya
        </h1>
        <div class="bdr"></div>
    </div>
    <div class="mid">
        <div class="place-more">
            <div class="cen" id="gc-1">
                @foreach ($newArticle as $at)
                    @include('web.article.card')
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

@endsection