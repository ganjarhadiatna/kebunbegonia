<div class="frm-big-galery">
    <div class="place">
        @foreach ($galery as $gl)
            <div 
                class="image image-all" 
                style="background-image: url({{ asset('/img/galery/thumbnails/'.$gl->cover) }});"></div>
        @endforeach
    </div>
</div>