@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="body-block">
    <div class="top">
        <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
            Galeri
        </p>
        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
            {{ $word }}
        </h1>
        <div class="bdr"></div>
    </div>
    @if (count($galery) != 0)
        <div class="padding-bottom-20px" style="text-align: center;">
            <div class="frm-tags">
                <a href="{{ url('/galeries') }}">
                    Galeri
                </a>
            </div>
            @foreach ($tags as $tg)
                <div class="frm-tags">
                    <a href="{{ url('/galeries/tags/'.strtolower(urlencode($tg->tag))) }}">
                        {{ $tg->tag }}
                    </a>
                </div>
            @endforeach
        </div>

        <div class="mid">
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @foreach ($galery as $gl)
                        @include('web.galery.card')
                    @endforeach
                </div>
            </div>
        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $galery->links() }}
            </div>
        </div>
    @else
        <div class="top">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                Galeri masih kosong
            </h1>
        </div>
    @endif
</div>

@endsection