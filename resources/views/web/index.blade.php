@extends('layouts.web')
@section('title', $title)
@section('content')
<script>
var slideIndex = 0;
var scrollBefore = $('.bn-main').width();

function hdScroll() {
    var top = $(this).scrollTop();
    var hg = $('#banner').height() - $('#other').height();
    if (top > hg) {
        $('#pl-header').removeClass('hd-bg-trans');
    } else {
        $('#pl-header').addClass('hd-bg-trans');
    }
}

function opVideo(stt) {
    var dt = '';
    if (stt == 'show') {
        $('#video-popup').fadeIn();
        $('#video-place').html(dt);
    } else {
        $('#video-popup').fadeOut();
        $('#video-place').html('');
    }
}

$(document).ready(function () {
    $('#banner').bxSlider({
        auto: true,
        autoHover: false,
        controls: false,
        autoControls: false,
        stopAutoOnClick: false,
        pager: true,
    });
    
    /*hdScroll();
    $(window).scroll(function(event) {
        hdScroll();
    });*/
});
</script>

<div class="body-padding"></div>

@include('web.main.image-popup')

<div class="frm-popup" id="video-popup">
    <div class="fp-place">
        <div class="close">
            <button 
                class="btn btn-main-color btn-radius" 
                onclick="opVideo('hide')">
                <span class="fa fa-lg fa-times"></span>
                <span>Tutup</span>
            </button>
        </div>
        <div class="fp-mid">
            <div class="vid" id="video-place"></div>
        </div>
    </div>
</div>

@if (count($banner) != 0)
    <div class="banner" id="banner" style="width: 100%;">
        @foreach ($banner as $bn)
            <div class="bn-place">
                <div class="bn-image image" style="background-image: url({{ asset('img/banner/covers/'.$bn->cover) }});"></div>
                @if (!empty($bn->title) || !empty($bn->description) || !empty($bn->link))
                    @if ($bn->position == 'center')
                        <?php $post = 'bn-center'; ?>
                    @endif
                    @if ($bn->position == 'bottom')
                        <?php $post = 'bn-bottom'; ?>
                    @endif
                    @if ($bn->position == 'disable')
                        <?php $post = 'bn-disable'; ?>
                    @endif
                    <div class="bn-cover {{ $post }}">
                        <div class="bn-info">
                            <div class="desc">
                                @if (!empty($bn->title))
                                    <h1>
                                        {{ $bn->title }}
                                    </h1>
                                @endif
                                @if (!empty($bn->description))
                                    <p>
                                        {{ $bn->description }}
                                    </p>
                                @endif
                            </div>
                            @if (!empty($bn->link))
                            <div class="more">
                                <a href="{{ $bn->link }}">
                                    <button class="btn btn-radius btn-main-color">
                                        Kunjungi
                                    </button>
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
    </div>
@endif

<div class="other bdr-top bdr-bottom">
    <ul class="menu">
        <li class="bdr-mobile">
            <div class="lef">
                <div class="fa fa-lg fa-calendar-alt"></div>
            </div>
            <div class="rig">
                <div class="ctn-main-font ctn-font-2 ctn-thin ctn-14pt">Jam Kerja</div>
                <div class="dsc">
                    Senin - Minggu : 08.00 - 17.00 WIB
                </div>
            </div>
        </li>
        <li class="bdr-mobile">
            <div class="lef">
                <div class="fa fa-lg fa-phone"></div>
            </div>
            <div class="rig">
                <div class="ctn-main-font ctn-font-2 ctn-thin ctn-14pt">
                    Kontak & Reservasi
                </div>
                <div class="dsc">
                    022 2788-527 & 081-2220-0202
                </div>
            </div>
        </li>
        <li>
            <div class="lef">
                <div class="fa fa-lg fa-map-marker-alt"></div>
            </div>
            <div class="rig">
                <div class="ctn-main-font ctn-font-2 ctn-thin ctn-14pt">Alamat</div>
                <div class="dsc">
                    Jalan Maribaya No. 120 Lembang, Bandung 40391
                </div>
            </div>
        </li>
    </ul>
</div>

@if (count($pinned) > 0)
    <div>
        <div class="body-block">
            <div class="mid">
                <div class="place-more">
                    <?php $post = ''; ?>
                    @if (count($pinned) == 1)
                        <?php $post = 'cen-1'; ?>
                    @endif
                    @if (count($pinned) == 2)
                        <?php $post = 'cen-2'; ?>
                    @endif
                    @if (count($pinned) == 3)
                        <?php $post = ''; ?>
                    @endif
                    <div class="cen {{ $post }}" id="gc-1">

                        <?php $i = 1; ?>
                        @if (count($pinned) == 1)
                            @foreach ($pinned as $at)
                                @include('web.article.card-small')
                                <?php $i += 1; ?>
                            @endforeach
                        @endif
                        @if (count($pinned) == 2)
                            @foreach ($pinned as $at)
                                @if ($i < 2)
                                    @include('web.article.card-pin')
                                @else
                                    @include('web.article.card-small')
                                @endif
                                <?php $i += 1; ?>
                            @endforeach
                        @endif
                        @if (count($pinned) >= 3)
                            @foreach ($pinned as $at)
                                @if ($i < 3)
                                    @include('web.article.card-pin')
                                @else
                                    @include('web.article.card-small')
                                @endif
                                <?php $i += 1; ?>
                            @endforeach
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if (count($service))
    <div>
        <!--konten & layanan-->
        <div class="body-block">
            <div class="top">
                <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                    Konten & Layanan
                </p>
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                    Apa yang kami sediakan?
                </h1>
                <div class="bdr"></div>
            </div>
            <div class="mid center">
                <div class="width width-all width-center">
                    @foreach ($service as $sv)
                        @include('web.service.card')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif

@if (count($note) > 0)
    <div>
        <!--konten & layanan-->
        <div class="body-block">
            <div class="top">
                <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                    Catatan & hal penting
                </p>
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                    Apa yang di dapatkan hari ini?
                </h1>
                <div class="bdr"></div>
            </div>
            <div class="mid center">
                <div class="place-more">
                    <div class="cen" id="gc-1">
                        @foreach ($note as $in)
                            <div class="frm-info">
                                <div 
                                    class="fi-top image image-full"
                                    style="background-image: url({{ asset('/img/note/thumbnails/'.$in->cover) }})">
                                    <div class="fi-cover"></div>
                                    <div class="fi-info">
                                        <div class="fi-icn {{ $in->icon }}"></div>
                                        <h2>{{ $in->title }}</h2>
                                        <p>{{ $in->description }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if (count($article) != 0)
    <div>
        <!--artikel-->
        <div class="body-block">
            <div class="top">
                <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                    Artikel
                </p>
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                    Berita & event terbaru
                </h1>
                <div class="bdr"></div>
            </div>
            <div class="mid">
                
                <div class="place-more">
                    <div class="cen" id="gc-1">
                        @foreach ($article as $at)
                            @include('web.article.card')
                        @endforeach
                    </div>
                </div>

                <div class="place-btn">
                    <a href="{{ url('/articles') }}">
                        <button class="btn btn-main-color btn-radius">
                            Lihat Artikel Lainnya
                            <span class="fa fa-lg fa-angle-right"></span>
                        </button>
                    </a>
                </div>

            </div>
        </div>
    </div>
@endif

@if (count($galery) != 0)
    <div>
        <!--galeri-->
        <div class="body-block">
            <div class="top">
                <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                    Galeri
                </p>
                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                    Kumpulan galeri terbaru kami
                </h1>
                <div class="bdr"></div>
            </div>
            <div class="mid">

                <div class="place-more">
                    <div class="cen" id="gc-1">
                        @foreach ($galery as $gl)
                            @include('web.galery.card')
                        @endforeach
                    </div>
                </div>

                <div class="place-btn">
                    <a href="{{ url('/galeries') }}">
                        <button class="btn btn-main-color btn-radius">
                                Lihat Galeri Lainnya
                            <span class="fa fa-lg fa-angle-right"></span>
                        </button>
                    </a>
                </div>

            </div>
        </div>    
    </div>
@endif

@if (count($testimony) != 0)
    <div>
        <!--testimoni-->
        <div class="body-block">
            <div 
                class="cvr" 
                style="background-image: url({{ asset('img/content/cc-7.jpg') }})"></div>
            <div class="padding-20px">
                <div class="top">
                    <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                        Testimoni
                    </p>
                    <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                        Review tentang Kebun begonia Lembang
                        <br>
                        yang diambil dari Trivadvisor
                    </h1>
                    <div class="bdr"></div>
                </div>
            </div>
            <div class="mid">

                <div class="place-more">
                    <div class="cen" id="gc-3">
                        @foreach ($testimony as $tt)
                            @include('web.testimonial.card')
                        @endforeach
                    </div>
                </div>

                <div class="place-btn">
                    <a href="https://www.tripadvisor.co.id/Attraction_Review-g7187358-d8857593-Reviews-Begonia_Garden-Lembang_West_Java_Java.html" target="_blank">
                        <button class="btn btn-main-color btn-radius">
                            Lihat Review Lainnya
                            <span class="fa fa-lg fa-angle-right"></span>
                        </button>
                    </a>
                </div>

            </div>
        </div>
    </div>
@endif

<div>
<!--
    <div class="body-block">
        <div class="top">
            <h1 class="title">
                Our Partners
            </h1>
            <div class="bdr"></div>
        </div>
        <div class="mid">
            <div class="place-more">
                <div class="cen" style="text-align: center;">
                    <div class="frm-sponsore">
                        <a href="{{ url('/') }}">
                            <div 
                                class="logo"
                                style="
                                    background-image: url({{ asset('img/sites/cocacode-2.png') }})
                                "></div>
                        </a>
                    </div>
                    <div class="frm-sponsore">
                        <a href="{{ url('/') }}">
                            <div 
                                class="logo"
                                style="
                                    background-image: url({{ asset('img/sites/tamukota.png') }})
                                "></div>
                        </a>
                    </div>
                    <div class="frm-sponsore">
                        <a href="{{ url('/') }}">
                            <div 
                                class="logo"
                                style="
                                    background-image: url({{ asset('img/sites/logo-center.png') }})
                                "></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="padding-20px">
        <div class="padding-5px"></div>
    </div>
-->

</div>

@endsection