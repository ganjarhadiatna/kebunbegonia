@extends('layouts.web')
@section('title', $title)
@section('content')

<?php 
    $src = $_GET['src'];
    $trg = $_GET['nav'];
    if ($trg == 'article') {
        $nav = '#nav-article';
    }
    if ($trg == 'galery') {
        $nav = '#nav-galery';
    }
?>

<script>
    $(document).ready(function () {
        $('{{ $nav }}').addClass('active');
    });
</script>

<div class="body-padding"></div>
<div class="body-block">
    <div class="top">
        <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
            Hasil pencarian
        </p>
        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color ctn-capital">
            kata kunci "{{ $src }}"
        </h1>
        <div class="bdr"></div>
    </div>
    <div>
        <div class="padding-10px"></div>
        
        <div class="padding-bottom-20px">
            <ul class="navigator">
                <a href="{{ url('/search?nav=article&src='.$src) }}">
                    <li class="left" id="nav-article">Artikel</li>
                </a>
                <a href="{{ url('/search?nav=galery&src='.$src) }}">
                    <li class="right" id="nav-galery">Galeri</li>
                </a>
            </ul>
        </div>

        <div class="padding-10px"></div>

        <div class="mid">
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @if ($trg == 'article')
                        @if (count($search) != 0)
                            @foreach ($search as $at)
                                @include('web.article.card')
                            @endforeach
                        @else
                            <div class="top">
                                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color ctn-capital">
                                    Artikel Tidak di Temukan
                                </h1>
                            </div>
                        @endif
                    @endif
                    @if ($trg == 'galery')
                        @if (count($search) != 0)
                            @foreach ($search as $gl)
                                @include('web.galery.card')
                            @endforeach
                        @else
                            <div class="top">
                                <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color ctn-capital">
                                    Galeri Tidak di Temukan
                                </h1>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $search->links() }}
            </div>
        </div>

    </div>
</div>

@endsection