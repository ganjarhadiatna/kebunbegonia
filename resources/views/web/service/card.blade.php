<div class="frm-service">
    <div>
        <div class="icn">
            <span class="fl {{ $sv->icon }}"></span>
        </div>
        <div class="ctn">
            <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-init-color">
                {{ $sv->title }}
            </div>
            <div class="dsc">
                {{ substr($sv->description, 0, 50).'...' }}
            </div>
            <div class="more">
                <a class="sb-link" href="{{ url('/service/'.strtolower(urlencode($sv->title))) }}">
                    <button class="btn btn-circle btn-sekunder-color">
                        <span class="fa fa-1x fa-plus"></span>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>