@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="body-block">
    <div class="top">
        @foreach ($service as $sv)
            <div class="width width-100px width-center padding-bottom-20px">
                <div class="icn">
                    <span class="fl {{ $sv->icon }}"></span>
                </div>
            </div>

            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                Konten & Layanan
            </p>

            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                {{ str_replace('_', ' ', $sv->title) }}
            </h1>

            <div class="bdr"></div>
            <div class="padding-5px"></div>

            <div class="width">
                <p class="desc text-normal">{{ $sv->description }}</p>
            </div>

            <div>
                <div class="padding-10px"></div>
                <div class="padding-bottom-20px">
                    <ul class="navigator">
                        <a href="{{ url('/service/article/'.strtolower(urlencode($sv->title))) }}">
                            <li class="left" id="nav-article">Artikel</li>
                        </a>
                        <a href="{{ url('/service/galery/'.strtolower(urlencode($sv->title))) }}">
                            <li class="right active" id="nav-galery">Galeri</li>
                        </a>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
    @if (count($galery) != 0)
        <div class="mid">
                
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @foreach ($galery as $gl)
                        @include('web.galery.card')
                    @endforeach
                </div>
            </div>

        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $galery->links() }}
            </div>
        </div>
    @else
        <div class="top">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                Galeri masih kosong
            </h1>
        </div>
    @endif
</div>

@endsection