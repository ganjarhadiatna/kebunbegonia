@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="body-block">
    <div class="top">
        @foreach ($service as $sv)
            <div class="width width-100px width-center padding-bottom-20px">
                <div class="icn">
                    <span class="fl {{ $sv->icon }}"></span>
                </div>
            </div>

            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
                Konten & Layanan
            </p>

            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                {{ str_replace('_', ' ', $sv->title) }}
            </h1>

            <div class="bdr"></div>
            <div class="padding-5px"></div>

            <div class="width">
                <p class="desc text-normal">{{ $sv->description }}</p>
            </div>
        @endforeach
    </div>
    @if (count($article) != 0)
        <div class="mid">
            <div class="place-more">
                @if (count($article) == 1)
                    <?php $post = 'cen-1'; ?>
                @endif
                @if (count($article) == 2)
                    <?php $post = 'cen-2'; ?>
                @endif
                @if (count($article) >= 3)
                    <?php $post = 'cen-3'; ?>
                @endif
                <div class="cen-col {{ $post }}" id="gc-1">
                    @foreach ($article as $at)
                        <div 
                            class="frm-today image" 
                            style="background-image: url({{ asset('/img/article/thumbnails/'.$at->cover) }}">
                            <div class="ft-cover">
                                <div class="ft-place">
                                    <div class="ft-top">
                                        <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-20pt ctn-white-color">
                                            {{ $at->title }}
                                        </h1>
                                    </div>
                                    <a href="{{ url('/article/'.base64_encode($at->idarticle)) }}">
                                        <div class="ft-mid fa fa-lg fa-angle-right"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $article->links() }}
            </div>
        </div>
    @endif
    @if (count($galery) != 0)
        <div class="mid">
            <div class="place-more">
                <div class="cen" id="gc-1">
                    @foreach ($galery as $gl)
                        @include('web.galery.card')
                    @endforeach
                </div>
            </div>
        </div>
        <div class="bot padding-top-10px">
            <div>
                {{ $galery->links() }}
            </div>
        </div>
    @endif
    @if ((count($galery) == 0) && (count($article) == 0))
        <div class="top">
            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
                Artikel & galeri masih kosong
            </h1>
        </div>
    @endif
</div>

@endsection