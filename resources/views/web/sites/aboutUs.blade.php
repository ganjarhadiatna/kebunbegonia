@extends('layouts.web')
@section('title', $title)
@section('content')

<div class="body-padding"></div>

<div class="about-us au-image"
	style="background-image: url({{ asset('img/sites/front.jpg') }})">
	<div class="au-bg-black"></div>
</div>

<div class="about-us">
	<div class="au-content">
		<div class="padding-20px"></div>
		<div class="grid grid-2 gap-50">
			<div class="col-1 au-info">
				<h1 class="ctn-main-font ctn-small ctn-sek-color ctn-thin ctn-font-2 ctn-line">
					Kebun Begonia Lembang
				</h1>
				
				<div class="bdr no-margin" style="float: left;"></div>
				<div class="padding-bottom-20px"></div>

				<p class="ctn-main-font ctn-sek-color ctn-font-3 ctn-11 ctn-line" style="text-align: justify;">
					Kebun Begonia Lembang adalah salah satu tempat wisata yang menyuguhkan aneka tanaman hias
					yang beraneka ragam dengan bunga "BEGONIA" yang menjadi ikon utamanya.
					Selain menyuguhkan tanaman hias dan udara lembang yang sejuk, Kebun Begonia Lembang juga
					memiliki berbagai fasilitas sperti: Kebun Bunga, Kebun Sayur, Cafe/Restoran, Toko Aksesoris,
					Pelatihan dan Reservasi Tempat.
				</p>
				<p class="padding-top-10px">
					<ul class="menu-circle">
	                    <li>
	                        <a href="https://www.instagram.com/kebunbegonia/" target="_blank">
	                            <span class="icn fab fa-lg fa-instagram"></span>
	                            <span class="ttl">Instagram</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="https://www.facebook.com/pages/Kebun-Begonia-Lembang/710603522325762" target="_blank">
	                            <span class="icn fab fa-lg fa-facebook"></span>
	                            <span class="ttl">Facebook</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="https://twitter.com/KebunBegonia" target="_blank">
	                            <span class="icn fab fa-lg fa-twitter"></span>
	                            <span class="ttl">Twitter</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="https://plus.google.com/105720139150647986705" target="_blank">
	                            <span class="icn fab fa-lg fa-google-plus"></span>
	                            <span class="ttl">Google+</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="https://www.youtube.com/channel/UCSoqhNAg0ffsPqB0uUPbAFQ" target="_blank">
	                            <span class="icn fab fa-lg fa-youtube"></span>
	                            <span class="ttl">Youtube</span>
	                        </a>
	                    </li>
	                </ul>
				</p>
			</div>
			<div class="col-2">
				<div 
					class="image image-full image-radius image-center"
					style="background-image: url({{ asset('img/sites/front.jpg') }})"></div>
			</div>
		</div>
		<div class="padding-20px"></div>
	</div>
</div>

<!--
<div class="about-us au-image"
	style="background-image: url({{ asset('img/sites/intro-1600.jpg') }})">
	<div class="au-bg-black"></div>
</div>

<div class="about-us">
	<div class="au-content">
		<div class="padding-20px"></div>
		<div class="grid grid-2 gap-50">
			<div class="col-1">
				<div 
					class="image image-full image-radius image-center"
					style="background-image: url({{ asset('img/sites/intro-1600.jpg') }})"></div>
			</div>
			<div class="col-2 au-info" style="text-align: right;">
				<h1 class="ctn-main-font ctn-small ctn-sek-color ctn-thin ctn-font-2 ctn-line">
					Website Baru
				</h1>
				
				<div class="bdr no-margin" style="float: right;"></div>
				<div class="padding-bottom-20px"></div>

				<p class="ctn-main-font ctn-sek-color ctn-font-3 ctn-11 ctn-line" style="text-align: justify;">
					Kebunbegonialembang.com adalah situs baru kami yang mulai diluncurkan pada
					bulan Oktober 2018. Situs ini berisikan informasi-informasi terbaru di
					Kebun Begonia Lembang seperti event, kumpulan galeri, artikel, layanan,
					pengumuman dan lain sebagainya. Situs ini di design dan di bangun oleh
					tim Coca Code Software, untuk informasi lebih lanjut silahkan kunjungi akun
					sosial media dibawah ini.
				</p>
				<p class="padding-top-10px">
					<ul class="menu-circle">
	                    <li>
	                        <a href="https://www.instagram.com/ganjar_hadiatna/" target="_blank">
	                            <span class="icn fab fa-lg fa-instagram"></span>
	                            <span class="ttl">Instagram</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="https://twitter.com/ganjarhadiatna" target="_blank">
	                            <span class="icn fab fa-lg fa-twitter"></span>
	                            <span class="ttl">Twitter</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="https://id.pinterest.com/ganjar_hadiatna/my-portofolio/" target="_blank">
	                            <span class="icn fab fa-lg fa-pinterest"></span>
	                            <span class="ttl">Pinterest</span>
	                        </a>
	                    </li>
	                </ul>
				</p>
			</div>
		</div>
		<div class="padding-20px"></div>
	</div>
</div>
-->

<div class="about-us au-image"
	style="background-image: url({{ asset('img/sites/2.jpg') }})">
	<div class="au-bg-black"></div>
</div>

<div class="about-us bdr-bottom">
	<div class="au-content">
		<div class="body-block">
			<div class="top">
	            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
	                Team
	            </p>
	            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
	                Kebun Begonia Lembang
	            </h1>
	            <div class="bdr"></div>
	        </div>
	        <div class="mid" style="text-align: center;">
		        
		        	<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Atmadi
					        </div>
					        <div class="dsc"></div>
					    </div>
					</div>

					<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Wahyudin
					        </div>
					        <div class="dsc"></div>
					    </div>
					</div>

					<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Hermawan
					        </div>
					        <div class="dsc"></div>
					    </div>
					</div>

					<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Nandar
					        </div>
					        <div class="dsc"></div>
					    </div>
					</div>

					<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Ahmad
					        </div>
					        <div class="dsc"></div>
					    </div>
					</div>
				
	        </div>
		</div>
	</div>
</div>

<div class="about-us">
	<div class="au-content">
		<div class="body-block">
			<div class="top">
	            <p class="ctn-main-font ctn-font-3 ctn-thin ctn-12pt ctn-sek-color">
	                Team
	            </p>
	            <h1 class="ctn-main-font ctn-font-2 ctn-thin ctn-18pt ctn-sek-color">
	                Web Developer
	            </h1>
	            <div class="bdr"></div>
	        </div>
	        <div class="mid" style="text-align: center;">
		        
		        	<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Ganjar Hadiatna
					        </div>
					        <div class="dsc">
					            Web Designer & Developer
					        </div>
					    </div>
					</div>

					<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Ramadhan Wijanarko
					        </div>
					        <div class="dsc">
					            Writer & Developer
					        </div>
					    </div>
					</div>

					<div class="frm-people">
					    <div class="padding-bottom-10px">
					        <div 
					        	class="image image-100px image-circle"
					        	style="
					        		background-image: url({{ asset('img/sites/3.jpg') }});
									margin: auto;
					        	"></div>
					    </div>
					    <div class="ctn">
					        <div class="ctn-main-font ctn-font-2 ctn-thin ctn-16pt ctn-sek-color">
					            Naufal Rifqi Fauzi
					        </div>
					        <div class="dsc">
					           	Writer & Developer
					        </div>
					    </div>
					</div>
				
	        </div>
		</div>
	</div>
</div>

<div class="about-us au-image"
	style="background-image: url({{ asset('img/sites/3.jpg') }})">
	<div class="au-bg-black"></div>
</div>

@endsection