@extends('layouts.web')
@section('title', $title)
@section('content')
<div class="body-padding"></div>
<div class="padding-20px">
	<div class="padding-15px"></div>
	<div class="informations">
		<h1 class="ctn-main-font ctn-mikro ctn-sek-color ctn-font-2 ctn-thin ctn-center ctn-line">
			FAQ
		</h1>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			<ol>
				<li class="ctn-main-font ctn-bold ctn-sek-color padding-bottom-20px">
					Berapa harga tiket masuk Kebun Bunga Begonia?
					<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
						Harga tiket Rp. 15.000,-/ orang (*mulai dari usia 2 tahun keatas), jika kamu membawa kamera akan terkena tarif Rp. 50.000. Paket prewed dengan harga Rp. 350.000,- / 2 jam.
					</p>
				</li>
				<li class="ctn-main-font ctn-bold ctn-sek-color padding-bottom-20px">
					Apakah Kebun Bunga Begonia buka setiap hari?
					<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
						Ya, Kebun Bunga Begonia buka setiap hari mulai dari Senin - Minggu : 08.00 - 17.00 WIB.
					</p>
				</li>
				<li class="ctn-main-font ctn-bold ctn-sek-color padding-bottom-20px">
					Apakah ada diskon potongan harga untuk rombongan?
					<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
						Ya, Untuk rombongan kamu bisa mendapatkan Diskon sebesar 10% untuk setiap rombongan dengan jumlah diatas 40 orang.
					</p>
				</li>
				<li class="ctn-main-font ctn-bold ctn-sek-color padding-bottom-20px">
					Apakah bisa membuat acara/event tertentu di Kebun Bunga Begonia?
					<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
						Ya, Kamu bisa menggelar acara apapun di Kebun Bunga Begonia. Mulai dari acara pengajian, arisan, pembagian rapor, syukuran, pelatihan, sampai dengan organ tunggal bisa Anda lakukan di Kebun Bunga Begonia.
					</p>
				</li>
				<li class="ctn-main-font ctn-bold ctn-sek-color padding-bottom-20px">
					Apakah ada tempat makan di Kebun Bunga Begonia?
					<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
						Ya, di Kebun Bunga Begonia terdapat Cafe yang menyediakan berbagai macam menu makanan.
					</p>
				</li>
				<li class="ctn-main-font ctn-bold ctn-sek-color padding-bottom-20px">
					Apakah boleh membawa makanan dari luar?
					<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
						Kami tidak memperbolehkan pengunjung membawa makanan dari luar untuk menjaga kebersihan lingkungan Kebun Bunga Begonia.
					</p>
				</li>
				<li class="ctn-main-font ctn-bold ctn-sek-color padding-bottom-20px">
					Apakah bus besar bisa masuk ke kawasn Kebun Bunga Begonia?
					<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
						Ya, kami menyediakan beberapa lahan untuk parkir bus.
					</p>
				</li>
			</ol>
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Tanggal Berlaku :
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebijakan ini terakhir diperbaharui pada 15 Oktober 2018.
		</p>
	</div>
	<div class="padding-15px"></div>
</div>
@endsection