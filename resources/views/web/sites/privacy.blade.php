@extends('layouts.web')
@section('title', $title)
@section('content')
<div class="body-padding"></div>
<div class="padding-20px">
	<div class="padding-15px"></div>
	<div class="informations">
		<h1 class="ctn-main-font ctn-mikro ctn-sek-color ctn-font-2 ctn-thin ctn-center ctn-line">
			Privacy
		</h1>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebijakan Privasi dan Keamanan Kebun Bunga Begonia ini menguraikan kebijakan dan praktik umum untuk jenis informasi yang dikumpulkan Kebun Bunga Begonia, bagaimana kami menggunakannya. informasi, dan pilihan yang dimiliki pengguna kami (” Anda ” atau ” Anda “) sehubungan dengan penggunaan kami dan kemampuan Anda untuk memperbaiki, informasi semacam itu.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Ini adalah prinsip privasi utama kami bahwa setiap informasi pribadi yang diberikan kepada kami oleh Anda hanya itu: pribadi dan pribadi. Dengan demikian, kami tidak akan pernah menjual, menyewakan, berbagi, atau mengungkapkan informasi pribadi Anda kepada siapapun kecuali untuk memberikan layanan kami atau sebagaimana dijelaskan dalam Kebijakan ini, tanpa memberikan pemberitahuan eksplisit mengenai hal tersebut dan kemampuan untuk tidak ikut serta.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Situs dan layanan Kebun Bunga Begonia tidak ditujukan untuk, atau dirancang untuk menarik, individu berusia di bawah delapan belas tahun. Kebun Bunga Begonia tidak secara sadar mengumpulkan informasi identitas pribadi dari orang mana pun di bawah usia delapan belas tahun
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Kebijakan Aplikasi :
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebijakan ini berlaku untuk informasi yang diberikan kepada kami oleh: (i) pengunjung situs web kami, (ii) pengguna layanan kami ( yaitu , pelanggan dari rencana kami), dan (iii) informasi yang kami kumpulkan dari pengunjung ke situs web kami pengguna. Kami umumnya hanya berfungsi sebagai saluran informasi yang dikendalikan oleh orang lain, dan karena itu Kebijakan ini mungkin tidak berlaku untuk situs web pengguna kami. Situs pengguna kami atau situs yang menyediakan tautan, mungkin memiliki kebijakan privasi mereka sendiri. Karena itu, kami sangat menganjurkan kesadaran dan pemahaman tentang kebijakan privasi terpisah situs web tersebut.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Pengumpulan Informasi dan Berbagi :
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebun Bunga Begonia adalah pemilik informasi layanan yang dikumpulkan di situs ini dan melalui Kebun Bunga Begonia. Saat pengunjung menjelajah situs web kami, atau situs web pengguna kami jika dilindungi oleh Kebun Bunga Begonia, kami biasanya log interaksi pengunjung ini untuk memberikan layanan yang lebih baik kepada pengguna kami (misalnya, menggunakan data log pengunjung untuk mendeteksi ancaman baru dan ketiga berbahaya pesta).
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Sebagai bagian dari layanan kami, kami dapat membuat laporan statistik berdasarkan apa yang pengunjung jahat telah mengunjungi situs tertentu, untuk lebih melindungi situs Anda. Kami juga dapat berbagi informasi tentang pengguna atau pengunjung mereka dimana kami memiliki keyakinan yang masuk akal bahwa mereka menimbulkan ancaman keamanan.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kami tidak menjual, menyewakan, atau berbagi informasi pribadi dengan pihak ketiga untuk tujuan pemasaran langsung mereka, termasuk yang ditentukan di bawah Kode Sipil.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Penegak Hukum dan Kewajiban :
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Mungkin saja kami diminta oleh hukum, perintah pengadilan, atau proses hukum lainnya untuk memberikan informasi tentang pelanggan kami kepada pihak luar. Ini adalah kebijakan kami untuk memastikan kepatuhan terhadap proses hukum yang berlaku dalam semua kasus tersebut, dan jika kami diminta untuk memberikan informasi dalam situasi ini, kami akan, jika memungkinkan, mencoba memberi tahu pengguna yang informasi yang harus kami hasilkan, kecuali dilarang oleh hukum.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kami juga dapat menyimpan salinan informasi pribadi untuk mematuhi kewajiban hukum kami, sesuai dengan kebijakan penyimpanan data kami, atau untuk jangka waktu yang wajar seperti yang diperlukan untuk mengatasi perselisihan potensial.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Tanggal Berlaku :
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebijakan ini terakhir diperbaharui pada 15 Oktober 2018.
		</p>
	</div>
	<div class="padding-15px"></div>
</div>
@endsection