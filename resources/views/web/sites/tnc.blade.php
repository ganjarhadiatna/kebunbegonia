@extends('layouts.web')
@section('title', $title)
@section('content')
<div class="body-padding"></div>
<div class="padding-20px">
	<div class="padding-15px"></div>
	<div class="informations">
		<h1 class="ctn-main-font ctn-mikro ctn-sek-color ctn-font-2 ctn-thin ctn-center ctn-line">
			Terms & Conditions
		</h1>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebun Bunga Begonia adalah sebuah perusahaan yang bergerak di bidang pariwisata dan menyediakan situs web ini (https://kebunbegonialembang.com/). Semua konten di bawah domain ini, perangkat lunak terkait dan layanannya, mengharuskan Anda untuk tunduk pada syarat dan ketentuan yang berlaku. Dengan menggunakan kebunbegonialembang.com/, Anda setuju untuk terikat dengan versi terbaru dari Perjanjian dan Kebijakan Privasi juga Keamanannya.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Menerima Persyaratan
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Dengan menggunakan informasi, alat, fitur dan fungsionalitas yang ada di kebunbegonialembang.com/ melalui API atau melalui perangkat lunak atau situs web lain yang berinteraksi dengan kebunbegonialembang.com/ atau API-nya (secara keseluruhan disebut “Layanan”), Anda setuju untuk terikat dengan Perjanjian ini, apakah Anda adalah “Pengunjung” (artinya Ada hanya melihat-lihat situs kebunbegonialembang.com/) atau Anda adalah “Anggota” (artinya Anda telah terdaftar dengan kebunbegonialembang.com/). Istilah “Anda” atau “Pengguna” mengacu pada Pengunjung atau Anggota.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Jika Anda ingin menjadi Anggota dan menggunakan layanan, Anda harus membaca Perjanjian ini dan menunjukan penerimaan Anda selama proses Pendaftaran. Jika Anda menerima Perjanjian ini, Anda menyatakan bahwa Anda memiliki kapasitas untuk terikat dengannya atau jika Anda bertindak atas nama perusahaan atau entitas, Anda memiliki wewenang untuk mengikat perusahaan atau entitas tersebut.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Hak Cipta
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Semua konten yang tersedia di kebunbegonialembang.com/, seperti teks, grafik, logo, ikon, tombol, gambar, klip audio, unduhan digital, kompilasi data dan perangkat lunak, serta kompilasi konten menjadi satu, situs yang koheren, adalah property Kebun Bunga Begoni (kebunbegonialembang.com/) dan dilindungi oleh undang-undang hak cipta Indonesia dan internasional. Dilarang memperbanyak isi kebunbegonialembang.com/ tanpa izin tertulis dari Kebun Bunga Begonia.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Lisensi Untuk Mengakses dan Layanan
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Sebagai pertimbangan untuk mengakses kebunbegonialembang.com/ dan Layanan, Kebun Bunga Begonia memberi Anda lisensi terbatas untuk mengakses dan memanfaatkan situs web secara pribadi. Lisensi ini melarang download Anda (selain caching halaman) atau memodifikasi bagiannya, kecuali dengan persetujuan tertulis dari Kebun Bunga Begonia. Lisensi ini tidak memungkinkan penjualan kembali layanan Kebun Bunga Begonia tanpa izin tertulis Kebun Bunga Begonia.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Anda mungkin tidak membingkai atau menggunakan teknik pembingkaian untuk melampirkan merek dagang, logo, atau informasi kepemilikan lainnya (termasuk gambar, teks, tata letak halaman, atau bentuk) kebunbegonialembang.com/ tanpa persetujuan tertulis dari Kebun Bunga Begonia. Anda mungkin tidak menggunakan tag meta atau “teks tersembunyi” lainnya dengan menggunakan nama dan merek Kebun Bunga Begonia tanpa persetujuan tertulis dari Kebun Bunga Begonia. Setiap penggunaan yang tidak sah secara otomatis menghentikan izin atau lisensi yang diberikan oleh Kebun Bunga Begonia dan dapat dikenakan kewajiban hukum untuk setiap kerusakan.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Anda diberi hak terbatas, dapat dibatalkan, dan tidak eksklusif untuk membuat hyperlink ke direktori yang tidak dilindungi kata sandi. Anda mungkin tidak menggunakan grafis eksklusif milik pemilik Kebun Bunga Begonia atau merek dagang sebagai bagian dari tautan tanpa izin tertulis.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Record Pengunjung dan Penyalahgunaan
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Sebagai pengunjung kebunbegonialembang.com dan pengguna Layanan Kebun Bunga Begonia, Anda setuju untuk membuat alamat Protokol Internet Anda dicatat dan aktivitas Anda dipantau untuk mencegah penyalahgunaan.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Record of Your Visitors
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Sebagai bagian dari Layanan Kebun Bunga Begonia, Anda dapat memilih untuk memantau lalu lintas ke situs web tertentu. Dengan demikian, Anda mengetahui bahwa Kebun Bunga Begonia bertindak sebagai agen terbatas Anda (dan pengolah data dalam konteks UE) kepada Anda untuk tujuan menyediakan layanan data dan pengoptimalan Internet. Anda mengetahui bahwa adalah tanggung jawab Anda untuk memastikan bahwa penggunaan Layanan Kebun Bunga Begonia diizinkan menurut undang-undang yurisdiksi Anda dan Anda setuju untuk mengganti kerugian dan menahan Kebun Bunga Begonia yang tidak berbahaya jika penggunaan Layanan oleh Anda melanggar undang-undang setempat.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Jika diminta oleh undang-undang, Anda setuju untuk mengeposkan kebijakan privasi atas hal itu, paling tidak, mengungkapkan setiap dan semua penggunaan informasi pribadi yang Anda kumpulkan dari pengguna termasuk informasi yang dikumpulkan melalui Layanan. Anda setuju untuk mengganti kerugian dan membela Kebun Bunga Begonia dari dan terhadap setiap dan semua klaim yang berasal dari kegagalan Anda untuk mematuhi ketentuan ini dan / atau kegagalan atau penolakan Anda untuk mematuhi persyaratan dan ketentuan dari setiap kebijakan privasi yang berlaku. Anda mengetahui bahwa Kebun Bunga Begonia dapat menggunakan data ini untuk memperbaiki layanannya atau mengaktifkan layanan lainnya (misalnya, menggunakan lalu lintas pengunjung atau data yang dikirim melalui layanan untuk mendeteksi ancaman sehingga dapat menghentikan serangan di masa depan).
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Hukum Yang Berlaku
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Dengan mengunjungi kebunbegonialembang.com/, Anda setuju bahwa undang-undang Amerika Serikat dan, khususnya, negara bagian California, tanpa memperhatikan prinsip-prinsip pertentangan hukum, akan mengatur Ketentuan Penggunaan ini dan setiap perselisihan apapun yang mungkin timbul. antara Anda dan Kebun Bunga Begonia atau afiliasinya. Setiap perselisihan atau tuntutan yang timbul dari atau sehubungan dengan Persetujuan ini harus diadili di San Francisco County, California, AS.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebun Bunga Begonia tidak menerima keluhan penyalahgunaan yang diajukan melalui telepon. Jika Anda memilih untuk tidak menggunakan formulir pengajuan keluhan kami, Anda dapat mengirimkan keluhan Anda ke:
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebun Bunga Begonia
			Jalan Maribaya No. 120 Lembang, Bandung 40391 
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Berikan informasi rinci yang mendukung keluhan Anda dan juga pernyataan tertulis yang membuktikan keabsahannya.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Dengan mengajukan keluhan, Anda mengetahui bahwa, atas kebijaksanaan Kebun Bunga Begonia sendiri, salinan keluhan tersebut mungkin diberikan kepada pengguna Kebun Bunga Begonia, penyedia hosting pengguna, yang diposkan di situs web Kebun Bunga Begonia.
		</p>
		<div class="padding-10px"></div>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kegagalan Kebun Bunga Begonia untuk melaksanakan atau memberlakukan hak atau ketentuan dalam Perjanjian ini tidak akan merupakan pengabaian hak atau ketentuan tersebut. Jika ada ketentuan dalam Persyaratan Layanan yang ditemukan oleh pengadilan dengan yurisdiksi yang kompeten tidak berlaku, namun para pihak setuju bahwa pengadilan harus berusaha untuk memberlakukan maksud para pihak sebagaimana tercermin dalam ketentuan, dan ketentuan lain dari Persyaratan Layanan tetap berlaku dan memiliki kekuatan penuh.
		</p>
		<div class="padding-10px"></div>
		<h2 class="ctn-main-font ctn-16pt ctn-sek-color ctn-font-2 ctn-thin ctn-line">
			Tanggal Berlaku :
		</h2>
		<p class="ctn-main-font ctn-13pt ctn-sek-color ctn-font-3 ctn-thin ctn-line">
			Kebijakan ini terakhir diperbaharui pada 15 Oktober 2018.
		</p>
	</div>
	<div class="padding-15px"></div>
</div>
@endsection