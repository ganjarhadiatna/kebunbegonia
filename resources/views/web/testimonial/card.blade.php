<div class="frm-testimonial">
	<div class="ft-ctn">
		<div class="icn fa fa-2x fa-quote-left"></div>
		<div class="say">
			<p style="font-family: 'Times New Roman', Times, serif;">
				{{ $tt->response }}
			</p>
		</div>
		<div class="inf">
			<div class="desc">
				<div class="name">{{ $tt->name }}</div>
				<div class="job">{{ $tt->job }}</div>
			</div>
		</div>
	</div>
</div>