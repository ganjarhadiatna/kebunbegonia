<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//web
Route::get('*', function () {
    return view('404', [
        'title' => '404 Not Found',
        'path' => 'none'
    ]);
});

Route::get('/', 'WebController@index');
Route::get('/instagram', 'WebController@instagram');
Route::get('/search', 'WebController@search');

//service
/*Route::get('/service/{ctr}', function ($ctr) {
    return redirect(url('/service/article/'.$ctr));
});*/
Route::get('/service/{ctr}', 'ServiceController@list');
Route::get('/service/article/{ctr}', 'ServiceController@listArticle');
Route::get('/service/galery/{ctr}', 'ServiceController@listGalery');

//article
Route::get('/article/{id}', 'ArticleController@view');
Route::get('/articles', 'ArticleController@list');

//galery
Route::get('/galeries', 'GaleryController@list');
Route::get('/galery/{idgalery}', 'GaleryController@view');
Route::get('/galeries/tags/{tag}', 'GaleryController@tags');

//banner

//sites
Route::get('/sites/about-us', 'WebController@aboutUs');
Route::get('/sites/terms-n-conditions', 'WebController@tnc');
Route::get('/sites/privacy', 'WebController@privacy');
Route::get('/sites/faq', 'WebController@faq');

//admin
//Auth::routes();
Route::get('/punten', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

//private
Route::middleware('auth')->group(function() {
    Route::get('/home', 'WebController@admin');
});

Route::middleware('auth')->prefix('admin')->group(function() {
    
    Route::get('/', 'WebController@admin');

    //banner
    Route::prefix('banner')->group(function () {
        Route::get('/', 'BannerController@index')->name('banner-index');
        Route::get('/create', 'BannerController@create')->name('banner-create');
        Route::get('/edit/{id}', 'BannerController@edit')->name('banner-edit');
        Route::post('/publish', 'BannerController@publish')->name('banner-publish');
        Route::post('/put', 'BannerController@put')->name('banner-put');
        Route::post('/remove', 'BannerController@remove')->name('banner-remove');
        Route::post('/changePosition', 'BannerController@changePosition');
    });

    //service
    Route::prefix('service')->group(function () {
        Route::get('/', 'ServiceController@index')->name('service-index');
        Route::get('/create', 'ServiceController@create')->name('service-create');
        Route::get('/edit/{id}', 'ServiceController@edit')->name('service-edit');
        Route::post('/publish', 'ServiceController@publish')->name('service-publish');
        Route::post('/put', 'ServiceController@put')->name('service-put');
        Route::post('/remove', 'ServiceController@remove')->name('service-remove');
    });

    //article
    Route::prefix('article')->group(function () {
        Route::get('/', 'ArticleController@index')->name('article-index');
        Route::get('/create', 'ArticleController@create')->name('article-create');
        Route::get('/edit/{id}', 'ArticleController@edit')->name('article-edit');
        Route::post('/publish', 'ArticleController@publish')->name('article-publish');
        Route::post('/put', 'ArticleController@put')->name('article-put');
        Route::post('/remove', 'ArticleController@remove')->name('article-remove');
        //draft
        Route::post('/changePinned', 'ArticleController@changePinned');
        Route::post('/changeDraft', 'ArticleController@changeDraft');
    });

    //galery
    Route::prefix('galery')->group(function () {
        Route::get('/', 'GaleryController@index')->name('galery-index');
        Route::get('/create', 'GaleryController@create')->name('galery-create');
        Route::get('/edit/{id}', 'GaleryController@edit')->name('galery-edit');
        Route::post('/publish', 'GaleryController@publish')->name('galery-publish');
        Route::post('/put', 'GaleryController@put')->name('galery-put');
        Route::post('/remove', 'GaleryController@remove')->name('galery-remove');
    });

    //note
    Route::prefix('note')->group(function () {
        Route::get('/', 'NoteController@index')->name('note-index');
        Route::get('/create', 'NoteController@create')->name('note-create');
        Route::get('/edit/{id}', 'NoteController@edit')->name('note-edit');
        Route::post('/publish', 'NoteController@publish')->name('note-publish');
        Route::post('/put', 'NoteController@put')->name('note-put');
        Route::post('/remove', 'NoteController@remove')->name('note-remove');
    });

    //testimony
    Route::prefix('testimony')->group(function () {
        Route::get('/', 'TestimonyController@index')->name('testimony-index');
        Route::get('/create', 'TestimonyController@create')->name('testimony-create');
        Route::get('/edit/{id}', 'TestimonyController@edit')->name('testimony-edit');
        Route::post('/publish', 'TestimonyController@publish')->name('testimony-publish');
        Route::post('/put', 'TestimonyController@put')->name('testimony-put');
        Route::post('/remove', 'TestimonyController@remove')->name('testimony-remove');
    });

    //admin
    Route::prefix('admin')->group(function () {
        Route::get('/', 'AdminController@index')->name('listAdmin');
        Route::get('/create', 'AdminController@create')->name('createAdmin');
        Route::get('/edit', 'AdminController@edit');
        Route::get('/password', 'AdminController@password');
        Route::post('/publish', 'AdminController@publish');
        Route::post('/photo', 'AdminController@photo');
        Route::post('/put', 'AdminController@put');
        Route::post('/password', 'AdminController@password');
        Route::post('/remove', 'AdminController@remove');
    });
});